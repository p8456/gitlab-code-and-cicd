# Gitlab Code and CICD

## Exercise 1

Scenario: Create a .gitlab-ci.yml file to see CI in action
In the project that you created earlier, add a new file.
Repository -> Files
Click + New File
Select .gitab-ci.yml
Apply Template Bash
Commit
Review the file

Click CI/CD
View the Pipeline that’s running
View the job from the pipeline

Echo contents of README file
Edit .gitlab-ci.yml
Copy and paste existing stage
```yaml
stage3:
  stage: test
  script:
  - cat README.md
```

- Code Templates
- Edit Tab
    Necesary keywords
    Define the job
    stage for this job
    Script for this job

    Visualize Tab
    Previsualizacion of Stages and Jobs

## Exercise 2

- Hacer un projecto en el gitlab local
- (Gitlab code mirroring) hacer un mirroring para cada vez que cambiemos algo en local, cambiarlo en .com

- (Runners)
    - hacer un pipleine dentro del grupo pipeline y luego hacer uno fuera del grupo para que no tenga runner

Cuando hablamos de los runners
 - mostrar las imagenes mas comunes de docker para lanzar comandos y hacer comparativa con los agentes y plugins en jenkins, buscar lista de plugins en Jenkins

VARIABLES cuando hablemos de las variables
 - la primera es definir variables en los jobs y poder verlas
 - proteger las variables con el proyecto y enmascarar
 - las variables que trae GITLAB por defecto ir a la ayuda
 - imprimir las variables y hablar un poco de ellas

Los repositorios 5
 - container registry
 - repository code
 - package registry
 - proxy registry
 - artifacts inside jobs
 - caches inside jobs and pipelines


## TOPICS
- Multiples jobs in the same stage
    - https://docs.gitlab.com/ee/ci/jobs/
- Relationship and dependencies with when between jobs
    - https://docs.gitlab.com/ee/ci/yaml/index.html#needs
```
linux:build:
  stage: build
  script: echo "Building linux..."

mac:build:
  stage: build
  script: echo "Building mac..."

lint:
  stage: test
  needs: []
  script: echo "Linting..."

linux:rspec:
  stage: test
  needs: ["linux:build"]
  script: echo "Running rspec on linux..."

mac:rspec:
  stage: test
  needs: ["mac:build"]
  script: echo "Running rspec on mac..."

production:
  stage: deploy
  script: echo "Running production..."
```
- Allow failure
```
    allow_failure: true
```
- Manual job
```
    when: manual
```
- Delay a Job
```
      stage: deploy
        script: echo 'Rolling out 10% ...'
        when: delayed
        start_in: 30 minutes
```
- RULES
```
    rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
- BEFORE, SCRIPT, AFTER SCRIPT
```
job2:
  before_script:
    - echo "Execute this script instead of the default `before_script`."
  script:
    - echo "This script executes after the job's `before_script`,"
    - echo "but the job does not use the default `after_script`."
  after_script: []
```
- ENVIRONMENT VARIABLES PROTECTING PASSWORDS
    - https://docs.gitlab.com/ee/ci/variables/
    Tokens or Passwords
    Go to Settings CICD Variables.
    Protected and Mask
    Another problem that we had on Jenkins, is that it show all the commands that we put in the command line, and the password where expoused.
    So in this way we protect and mask our passwords  etc
    ![image.png](./image.png)
    ![image-1.png](./image-1.png)
- Download Artifacts
    ![image-2.png](./image-2.png)
- Shared Runners
    Create a Specific Runner
    Create a Shared Runner
    ![image-3.png](./image-3.png)
- Adding the artifacts
    The failure
```
        - The reason of failure
    Each job is a Frash instance of a container, actually a runner container
    The don’t comparten los ficheros de los jobs previous, 
    they have a separeta workspace
    Difference with Jenkins

    In easy pipelines this could be una desventaja, pero
    la verdadera ventyaja esta en poder lanzar un runner con imagenes de docker customizadas con un entorno independiente cada vez, por ejemplo, un job lanzando commandos de AWS en la nube, uno lanzando commandos de Kubernetes en nuestro cluster, luego otro runner lanzando commandos de ubuntu, la velocidad y rapidez con la que se levantan los runners  ejecutan codiggo y se destruyen para instanciar uno nuevo es mucho major.

    La solucion, los artifacts.
    Son almacenes para compartir esta informacion de un job a otro.
    Realmente temenos hasta 5 tipo de almacenes en gitlab, Pakcage registry, container registry, code registry, artifacts, and cach’e
```
    - https://docs.gitlab.com/ee/ci/migration/jenkins.html
    The runner that execute this job actually upload the artifacts to gitlab, 
    we will see in the future how we can to download this artifacts from the UI
    The job donload the artifacts and show the content
    We can put the duration of the timestamp
    ![image-4.png](./image-4.png)
    ![image-5.png](./image-5.png)

## Exercise 3


En El laboratorio de Portugal
El propio pipeline de la web lanzarlo, cambiarle las imagenes de inicio y desplegar de nuevo el pipeline de la WEB


